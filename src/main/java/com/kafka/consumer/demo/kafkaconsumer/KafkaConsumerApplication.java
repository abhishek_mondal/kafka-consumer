package com.kafka.consumer.demo.kafkaconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.kafka.consumer.demo")
public class KafkaConsumerApplication {

	public static void main(String[] args) {

		SpringApplication.run(KafkaConsumerApplication.class, args);
	}

}
