package com.kafka.consumer.demo.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.CountDownLatch;

@Service
public class KakaMessageReceiver {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(KakaMessageReceiver.class);

    @KafkaListener(topics = "testTopic", groupId = "first-group")
    public void listen(@Payload String payload,
                       @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partitionId) {

        LOGGER.info("Received payload : {} \n with partition id : {}", payload, partitionId);
    }

    @PostConstruct
    public void consumerSetup() {
        LOGGER.info("Consumer is ready ..");
    }
}
